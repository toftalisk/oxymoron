package oxymoron.everything;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import oxymoron.everything.ws.FactoryAbstractServletFactoryBeanShit;

public class Main {

    public static void main(String[] args) throws Exception{
        Server server = new Server();
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(8080);
        server.addConnector(connector);

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        context.setResourceBase("public");
        server.setHandler(context);

        ServletHolder holderEvents = new ServletHolder("ws-events", FactoryAbstractServletFactoryBeanShit.class);
        context.addServlet(holderEvents, "/game");

        DefaultServlet files = new DefaultServlet();
        context.addServlet(new ServletHolder(files), "/");

        server.start();
        server.join();
    }


}
