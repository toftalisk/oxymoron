package oxymoron.everything.protocols;

import oxymoron.everything.game.GameObject;
import oxymoron.everything.game.Vec2d;

public interface AssProtocol {
    void register(String imgId, String imgPath);
    void register(String imgId, String imgPath, int frameCount, int fps, boolean loop);
    void create(GameObject object);
    void update(GameObject object);
    void updateImage(String objectId, String image);
    void delete(String id);
    void camera(Vec2d center);
    void drawJavascript();
}
