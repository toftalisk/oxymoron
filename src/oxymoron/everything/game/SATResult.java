package oxymoron.everything.game;

/**
 * Created by are on 25.01.15.
 */
public class SATResult {
    private Vec2d axis;
    private Range overlap;
    public SATResult(Vec2d axis, Range overlap)
    {
        this.axis = axis;
        this.overlap = overlap;
    }
    public Vec2d getAxis() { return axis; }
    public Range getOverlap() { return overlap; }
}
