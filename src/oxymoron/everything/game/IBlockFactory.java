package oxymoron.everything.game;


import java.util.List;

public interface IBlockFactory {
    public List<Block> getNextColumn();
}
