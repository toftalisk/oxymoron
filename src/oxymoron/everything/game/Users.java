package oxymoron.everything.game;

import oxymoron.everything.ws.WebSocketConnection;

import java.util.HashMap;
import java.util.Map;

public class Users {
    static Map<String, WebSocketConnection> users = new HashMap<>();
    public static void add(String id, WebSocketConnection eventSocket) {
        users.put(id, eventSocket);
    }

    public static void drop(String id) {
        users.remove(id);
    }
}
