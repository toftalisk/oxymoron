package oxymoron.everything.game.gameobjects;

import oxymoron.everything.game.GameObject;
import oxymoron.everything.game.DynamicGameObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GameObjectMap<T extends GameObject> extends HashMap<String, T> {

    public void add(T obj) {
        put(obj.getId(), obj);
    }

    public List<DynamicGameObject> physical() {
        List<DynamicGameObject> obs = new ArrayList<>();
        for(GameObject o : values()){
            if(o instanceof DynamicGameObject) obs.add((DynamicGameObject) o);
        }
        return obs;
    }
}
