package oxymoron.everything.game.gameobjects;


import oxymoron.everything.game.*;
import oxymoron.everything.game.gameobjects.projectile.Fireball;
import oxymoron.everything.game.gameobjects.projectile.ProtonBomb;

import java.util.EnumSet;

public class GameCharacter extends DynamicGameObject
{
    public enum Action
    {
        MoveRight,
        MoveLeft,
        Stop,
        ShootFireball,
        Jump,
        DigDown,
        StopDig,
        SetBomb
    }

    private EnumSet<Action> actions = EnumSet.noneOf(Action.class);
    private boolean onGround;
    private CardinalDirection facing;
    private int digstate=0;
    private Block lastStandonBlock = null;
    private final int WALKING__SPEED = 300;
    private boolean shouldFuckUp = false;


    public GameCharacter() {
        super(new Vec2d());
        setAffectedByGravity(true);
        setVelocity(new Vec2d(0, 0));
        addCollisionFixture(new Vec2d(100, 240), CollisionFixture.Layer.Character, EnumSet.of(CollisionFixture.Layer.Blocks, CollisionFixture.Layer.Character, CollisionFixture.Layer.Enemies));
        onGround = false;
        shouldFuckUp = false;
    }

    public boolean getShouldFuckUp()
    {
        boolean tmp = shouldFuckUp;
        shouldFuckUp = false;
        return tmp;
    }

    @Override
    public String getImage() {
        return "still";
    }

    @Override
    public boolean onCollision(Vec2d direction, GameObject theOtherThing)
    {
        if ((direction.y > 0.0f) && (Math.abs(direction.y) > Math.abs(direction.x))) {
            landed();
            if(theOtherThing instanceof Block) {
                 lastStandonBlock = (Block) theOtherThing;
            }
        }

        if (theOtherThing instanceof  Ox) {
            shouldFuckUp = true;
        }
        return true;
    }

    public void landed(){
        onGround = true;
    }

    protected CardinalDirection getFacing()
    {
        return facing;
    }

    // Perform this action on next update
    public void perform(Action a)
    {
        actions.add(a);
    }

    @Override
    public void update(WorldAction worldAction, double time, double delta)
    {
        super.update(worldAction, time, delta);
        for (Action a : actions)
        {
            switch(a)
            {
                case MoveLeft:
                    if (onGround)
                        worldAction.changeImage("run");
                    setVelocity(-WALKING__SPEED, getVelocity().y);
                    facing = CardinalDirection.LEFT;
                    break;
                case MoveRight:
                    if (onGround)
                        worldAction.changeImage("run");
                    setVelocity(+WALKING__SPEED, getVelocity().y);
                    facing = CardinalDirection.RIGHT;
                    break;
                case Stop:
                    if (onGround)
                        worldAction.changeImage("still");
                    setVelocity(0, getVelocity().y);
                    break;
                case ShootFireball:
                    Fireball f = new Fireball(getPos(), CardinalDirection.pickOne());
                    worldAction.SpawnObject(f);
                    break;
                case Jump:
                    if (onGround)
                    {
                        onGround = false;
                        accelBy(0, -800);
                        worldAction.changeImage("jump");
                    }
                    break;
                case DigDown:
                    if (onGround && lastStandonBlock != null && digstate==3)
                    {
                        worldAction.DestroyObject(lastStandonBlock);
                        lastStandonBlock = null;
                    }
                    digstate = Math.min(digstate + 1, 3);
                    worldAction.changeImage("dig" + digstate);
                    break;
                case StopDig:
                    digstate = Math.max(digstate - 1, 0);
                    if(digstate > 0)
                        worldAction.changeImage("dig" + digstate);
                    break;
                case SetBomb:
                    worldAction.SpawnObject(new ProtonBomb(getPos(), 5));
                    break;
            }
        }
        actions.clear();
    }
    public int getDigstate() {
        return this.digstate;
    }
}
