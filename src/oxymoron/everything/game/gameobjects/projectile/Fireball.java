package oxymoron.everything.game.gameobjects.projectile;

import oxymoron.everything.game.*;
import oxymoron.everything.game.gameobjects.explosions.ProtonExplosion;

import java.util.EnumSet;

public class Fireball extends DynamicGameObject
{
	private GameObject collisionTarget = null;

	@Override
	public String getImage()
	{
		return "fireball";
	}

	public Fireball(Vec2d pos, CardinalDirection dir)
	{
		super(pos);
		setAffectedByGravity(false);
		setAngle(dir.Angle);
		setVelocity(Vec2d.fromAngle(dir.Angle, 200.0));
		addCollisionFixture(new Vec2d(50,50), CollisionFixture.Layer.Projectiles, EnumSet.of(CollisionFixture.Layer.Blocks, CollisionFixture.Layer.Enemies));
	}

	public boolean onCollision(Vec2d direction, GameObject theOtherThing)
	{
		collisionTarget = theOtherThing;
		return true;
	}

	@Override
	public void update(WorldAction action, double time, double delta) {
		super.update(action, time, delta);
		if (collisionTarget != null) {
			action.DestroyObject(collisionTarget);
			action.DestroyObject(this);
			action.SpawnObject(new ProtonExplosion(getPos()));
		}
	}
}
