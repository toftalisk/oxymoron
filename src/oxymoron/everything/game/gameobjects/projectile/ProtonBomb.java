package oxymoron.everything.game.gameobjects.projectile;

import oxymoron.everything.game.*;
import oxymoron.everything.game.gameobjects.explosions.ProtonExplosion;

import java.util.EnumSet;

public class ProtonBomb extends DynamicGameObject
{
    private double timer;

    public ProtonBomb(Vec2d pos, double timer) {
        super(pos);
        this.timer = timer;
        addCollisionFixture(new Vec2d(20, 20), CollisionFixture.Layer.Projectiles, EnumSet.of(CollisionFixture.Layer.Projectiles, CollisionFixture.Layer.Blocks));
    }

    @Override
    public String getImage() {
        return "proton_bomb";
    }

    @Override
    public boolean isAffectedByGravity() {
        return true;
    }

    @Override
    public void update(WorldAction action, double time, double delta) {
        super.update(action, time, delta);

        timer -= delta;
        if (timer < 0)
        {
            action.SpawnObject(new ProtonExplosion(getPos()));
            Vec2d offset = getPos().add(0, 60);
            for (CardinalDirection c : CardinalDirection.values())
                action.SpawnObject(new PlasmaBall(offset, c));
            action.DestroyObject(this);
        }
    }
}
