package oxymoron.everything.game.gameobjects.projectile;

import oxymoron.everything.game.*;
import oxymoron.everything.game.gameobjects.explosions.ProtonExplosion;

import java.util.EnumSet;

public class PlasmaBall extends DynamicGameObject {
    private GameObject collisionTarget = null;
    private double life = 2.0;

    @Override
    public String getImage()
    {
        return "plasmaball";
    }

    public PlasmaBall(Vec2d pos, CardinalDirection dir)
    {
        super(pos);
        setAffectedByGravity(false);
        setAngle(dir.Angle);
        setVelocity(Vec2d.fromAngle(dir.Angle, 200.0));
        addCollisionFixture(new Vec2d(50,50), CollisionFixture.Layer.Projectiles, EnumSet.of(CollisionFixture.Layer.Blocks, CollisionFixture.Layer.Enemies, CollisionFixture.Layer.Character));
    }

    public boolean onCollision(Vec2d direction, GameObject theOtherThing)
    {
        collisionTarget = theOtherThing;
        return false;
    }

    @Override
    public void update(WorldAction action, double time, double delta) {
        super.update(action, time, delta);
        if (collisionTarget != null) {
            if (!(collisionTarget instanceof Block)) {
                action.DestroyObject(this);
                action.SpawnObject(new ProtonExplosion(getPos()));
            }
            action.DestroyObject(collisionTarget);
        }
        life -= delta;
        if (life < 0)
            action.DestroyObject(this);
    }
}
