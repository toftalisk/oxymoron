package oxymoron.everything.game.gameobjects.explosions;

import oxymoron.everything.game.GameObject;
import oxymoron.everything.game.Vec2d;
import oxymoron.everything.game.WorldAction;

public class ProtonExplosion extends GameObject
{
    private static final double LIFETIME = 1.0;
    private double life = LIFETIME;

    public ProtonExplosion(Vec2d pos) {
        super(pos);
    }

    @Override
    public String getImage() {
        return "proton_explosion";
    }

    @Override
    public void update(WorldAction action, double time, double delta) {
        super.update(action, time, delta);

        life -= delta;
        if (life < 0)
            action.DestroyObject(this);
    }
}
