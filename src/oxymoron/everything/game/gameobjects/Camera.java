package oxymoron.everything.game.gameobjects;

import oxymoron.everything.game.Block;
import oxymoron.everything.game.BlockFactory;
import oxymoron.everything.game.Vec2d;

public class Camera {
    private Handler handler;
    private int w = 2000;
    private int h = 1000;

    public void setSize(int w, int h) {
        this.w = w;
        this.h = h;
    }

    public interface Handler {
        void onCameraMove(Vec2d camera_pos);
    }

    public Camera(Handler handler) {
        this.handler = handler;
    }

    private Vec2d camera_pos = new Vec2d(0.0, 0.0);

    public Vec2d getPos(){
        return camera_pos;
    }

    public void moveToRelative(Vec2d rel)
    {
        camera_pos = Vec2d.add(camera_pos, rel);
        handler.onCameraMove(camera_pos);
    }

    public Vec2d min() {
        Vec2d camera_size = new Vec2d(w, h);
        Vec2d camera_half_size = camera_size.div(2.0f);
        return Vec2d.sub(camera_pos, camera_half_size);
    }

    public Vec2d max() {
        Vec2d camera_size = new Vec2d(w, h);
        Vec2d camera_half_size = camera_size.div(2.0f);
        return Vec2d.add(camera_pos, camera_half_size);
    }
}
