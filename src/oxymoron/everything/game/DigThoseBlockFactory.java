package oxymoron.everything.game;


import oxymoron.everything.game.tileSets.TileSet;
import oxymoron.everything.game.tileSets.TileType;

import java.util.ArrayList;
import java.util.List;

public class DigThoseBlockFactory implements IBlockFactory{
    int currentHeight = 2;
    int currentX = 0;
    int abyss = 0;

    int digging_this_state=0;
    int abyss_state=0;
    int distance_rows = 4;
    int smooth_start = 30;

    private List<Block> createColumn(int x, int y_from, int y_to) {
        List<Block> ret = new ArrayList<Block>();
        for (int i = y_from; i <= y_to; i++) {
            String tileId = TileSet.getIdString(TileType.MIDDLE);
            if (i == y_from) {
                tileId = TileSet.getIdString(TileType.TOP);
            }
            ret.add(new Block(tileId, new Vec2d(x * Block.HEIGHT, i * Block.HEIGHT)));
        }

        return ret;
    }

    private List<Block> digCreateColumn(int x, int y_from, int y_to) {
        List<Block> ret = new ArrayList<Block>();
        if (digging_this_state > 1) {//add a platform below. We should dig to this
            ret.addAll(createColumn(x, y_from, y_from +1)); //first platform
            ret.addAll(createColumn(x, y_from+distance_rows, y_to));//second platform

        } else if(digging_this_state == 1) {//Block highest path so we need to dig
            ret.addAll(createColumn(x, y_from-40, y_from)); //block
            ret.addAll(createColumn(x, y_from+distance_rows, y_to)); //second platform
        }
        return ret;
    }

    public List<Block> getNextColumn() {

        smooth_start--;

        if (smooth_start <= 0) {
            if ((Math.random() > 0.90) && abyss_state == 0) {
                digging_this_state = 3;
            }

            if (digging_this_state == 0 && abyss_state == 0 && Math.random() > 0.95) {
                abyss_state = 4;
            }
        }

        List<Block> blocks = new ArrayList<Block>();
        if(digging_this_state>0) {
            blocks = digCreateColumn(currentX, currentHeight, 40);
            digging_this_state--;
            if (digging_this_state == 0) {
                //return to normal
                currentHeight += distance_rows;
            }
        }else if (abyss_state <= 2) {
            blocks = createColumn(currentX, currentHeight, 40);
        }
        if (abyss_state > 0)
            abyss_state --;



        currentX++;

        if (Math.random() > 0.8) {
            if (Math.random() > 0.8) {
                currentHeight++;//level moving down
            } else {
                currentHeight--;//level moving up
            }
        }

        return blocks;
    }

}
