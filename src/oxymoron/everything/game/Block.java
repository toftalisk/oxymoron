package oxymoron.everything.game;

import java.util.EnumSet;

public class Block extends GameObject
{
	private final String blockSprite;

	public static final int HEIGHT = 128;
	public static final int WIDTH = 128;

	public Block(String blockSprite, Vec2d pos) {
        super(pos);
		this.blockSprite = blockSprite;
		addCollisionFixture(new Vec2d(WIDTH, HEIGHT), CollisionFixture.Layer.Blocks, EnumSet.noneOf(CollisionFixture.Layer.class));
	}

	public String getImage() {
		return blockSprite;
	}
}
