package oxymoron.everything.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class WorldAction
{
	private List<GameObject> spawnList = new ArrayList<GameObject>();
	private List<String> destroyList = new ArrayList<String>();
	private String newImage = null;
	private Vec2d explosionPos = null;
	private double explosionRadius;

	public WorldAction() {
	}

	public void SpawnObject(GameObject o)
	{
		spawnList.add(o);
	}

	public void DestroyObject(GameObject o)
	{
		destroyList.add(o.getId());
	}

	public void changeImage(String newImage)
	{
		this.newImage = newImage;
	}

	public void explode(Vec2d pos, double radius)
	{
		explosionPos = pos;
		explosionRadius = radius;
	}

	public List<GameObject> getSpawnList()
	{
		return spawnList;
	}

	public List<String> getDestroyList()
	{
		return destroyList;
	}

	public boolean isImageChanged()
	{
		return newImage != null;
	}

	public String getNewImage()
	{
		return newImage;
	}
}
