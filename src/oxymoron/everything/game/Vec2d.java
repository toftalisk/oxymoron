package oxymoron.everything.game;

public class Vec2d
{
	public final double x;
	public final double y;


    public Vec2d() {
        this(0,0);
    }
    public Vec2d(Vec2d v)
    {
        x = v.x;
        y = v.y;
    }

	public Vec2d(double x, double y) {
		this.x = x;
		this.y = y;
	}

    public double lengthSq() { return (x*x) + (y*y); }
    public Vec2d add(Vec2d v0) {
        return Vec2d.add(this,v0);
    }

    public Vec2d addX(int x) {
        return new Vec2d(this.x + x, y);
    }

    public Vec2d add(int x, double y) {
        return this.add(new Vec2d(x,y));
    }

    public double dot(Vec2d v) { return (x * v.x) + (y * v.y); }

    public boolean containedWithin(Vec2d min, Vec2d max)
    {
        if ((min.x <= x)&&(min.y <= y)&&(max.x >= x)&&(max.y >= y)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "(" + x +"," + y + ")";
    }
    public static Vec2d add(Vec2d a, Vec2d b)
    {
        return new Vec2d(a.x + b.x, a.y + b.y);
    }
    public static Vec2d sub(Vec2d a, Vec2d b)
    {
        return new Vec2d(a.x - b.x, a.y - b.y);
    }
    public Vec2d div(double v)
    {
        return new Vec2d(x / v, y / v);
    }
    public Vec2d mul(double v)
    {
        return new Vec2d(x * v, y * v);
    }

    public static Vec2d fromAngle(double angle, double magnitude) {
        return new Vec2d(Math.cos(angle) * magnitude, -Math.sin(angle) * magnitude);
    }


    public double toAngle()
    {
        double angle = (Math.PI * 0.5f);

        if (Math.abs(x) > 0.1) {
            angle = Math.atan(y / x);
            if (x < 0.0f) {
                angle = Math.PI - angle;
            }
        }
        return angle;
    }
}
