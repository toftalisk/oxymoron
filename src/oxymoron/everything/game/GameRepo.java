package oxymoron.everything.game;

import oxymoron.everything.protocols.AssProtocol;

import java.util.HashMap;
import java.util.Map;

public class GameRepo {

    private static int id_counter = 0;

    public static Map<String, WebSocketGame> games = new HashMap<>();

    public static String getNextId() {
        return Integer.toString(id_counter++);
    }

    public static WebSocketGame create(AssProtocol ass) {
        String id = getNextId();

        MyGame game = new MyGame(id, ass);
        game.startGame();

        games.put(id, game);

        return game;
    }

    public static WebSocketGame join(String userId, String gameId) {
        WebSocketGame game = games.get(gameId);

        if(game != null) {
            game.addUser(userId);
            return game;
        }
        return null;
    }
}
