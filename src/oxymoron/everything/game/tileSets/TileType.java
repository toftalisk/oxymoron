package oxymoron.everything.game.tileSets;


public enum TileType {


    TOP,
    TOP_INNER_CORNER,
    TOP_CORNER,
    MIDDLE,
    MIDDLE_ALT,
    SIDE,
    BOT,
    BOT_INNER_CORNER,
    BOT_CORNER
}
