package oxymoron.everything.game.tileSets;


import oxymoron.everything.protocols.AssProtocol;

import java.util.HashMap;
import java.util.Map;

//Make this abstract and extend this when we have multiple tile sets....for now just making the one.
public class TileSet {

    private static String name = "snow";
    private static String basePath = "Graphics/Snow_Tileset/";


    public static void registerTiles(AssProtocol protocol){


        for(TileType tileType : TileType.values()){
            regTile(protocol, getIdString(tileType) , basePath + tileType.toString().toLowerCase() + ".png");
        }

    }

    private static void regTile(AssProtocol protocol, String idString, String fileName){

        protocol.register(idString, fileName);
    }

    public static String getIdString(TileType type)
    {
        return name + type.toString();
    }

}
