package oxymoron.everything.game;

public class MessageEncorder {

    public static String encode(Object ... args) {
        StringBuilder sb = new StringBuilder();
        for(Object arg: args) sb.append(arg).append(":");
        return sb.substring(0, sb.length()-1);
    }


    public static String encodeLog(Object ... args) {
        String str = encode(args);
        System.out.println(str);
        return str;
    }
}
