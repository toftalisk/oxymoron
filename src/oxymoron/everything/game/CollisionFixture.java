package oxymoron.everything.game;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class CollisionFixture {
    public enum Layer
    {
        None,
        Blocks,
        Character,
        Projectiles,
        Enemies
    }

    private Layer layer;
    private EnumSet<Layer> collisionMask;
    private List<Vec2d> axesOfSeparation = new ArrayList<Vec2d>();
    private List<Vec2d> points = new ArrayList<Vec2d>();
    private Vec2d size;

     Range projectOntoAxis(Vec2d axis, Transform transform) {
         if (points.size() == 0) {
             return new Range(0.0f);
         }
         Range current = new Range(axis.dot(transform.transformPoint(points.get(0))));

         for (Vec2d p : points) current.extend(axis.dot(transform.transformPoint(p)));

         return current;
     }

    public boolean canCollideWith(CollisionFixture other)
    {
        return collisionMask.contains(other.layer);
    }

    public static SATResult getAxisOfSeparation(
            CollisionFixture fixtureA, Transform transformA,
            CollisionFixture fixtureB, Transform transformB
    ) {
        Range best_range = null;
        Vec2d best_axis = null;

        List<Vec2d> transformed_axes = new ArrayList<Vec2d>();

        for (Vec2d axis : fixtureA.axesOfSeparation) {
            transformed_axes.add(transformA.transformVector(axis));
        }

        for (Vec2d axis : fixtureB.axesOfSeparation) {
            transformed_axes.add(transformB.transformVector(axis));
        }

        for (Vec2d axis : transformed_axes) {

            Range rangeA = fixtureA.projectOntoAxis(axis, transformA);
            Range rangeB = fixtureB.projectOntoAxis(axis, transformB);
            Range overlap = Range.overlap(rangeA, rangeB);

            // We found a separating axis. Exit!
            if (overlap.getSize() < 0.0f)
                return null;

            if (Math.abs(rangeB.getMin() - rangeA.getMax()) > Math.abs(rangeA.getMin() - rangeB.getMax())) {
                axis = axis.mul(-1.0f);
            }


            if ((best_range == null) || (overlap.getSize() < best_range.getSize())) {
                best_axis = axis;
                best_range = overlap;
            }
        }

        if (best_range == null) {
            return null;
        }
        return new SATResult(best_axis, best_range);
    }

    static CollisionFixture createBox(Vec2d size, Layer layer)
    {
        return createBox(size, layer, EnumSet.allOf(Layer.class));
    }

    static CollisionFixture createBox(Vec2d size, Layer layer, EnumSet<Layer> mask)
    {
        CollisionFixture cf = new CollisionFixture();
        cf.layer = layer;
        cf.collisionMask = mask;
        cf.size = size;
        cf.axesOfSeparation.add(new Vec2d(1.0f, 0.0f));
        cf.axesOfSeparation.add(new Vec2d(0.0f, 1.0f));

        cf.points.add(new Vec2d(+size.x / 2.0, +size.y / 2.0));
        cf.points.add(new Vec2d(+size.x / 2.0, -size.y / 2.0));
        cf.points.add(new Vec2d(-size.x / 2.0, +size.y / 2.0));
        cf.points.add(new Vec2d(-size.x / 2.0, -size.y / 2.0));


        return cf;
    }

    public Vec2d getSize() {
        return size;
    }
}
