package oxymoron.everything.game;

import java.util.Random;

public enum CardinalDirection
{
	RIGHT(0/8d),
	DOWN_RIGHT(1/8d),
	DOWN(2/8d),
	DOWN_LEFT(3/8d),
	LEFT(4/8d),
	UP_LEFT(5/8d),
	UP(6/8d),
	UP_RIGHT(7/8d);

	public final double Angle;
	CardinalDirection(double units)
	{
		Angle = Math.PI * 2.0d * units;
	}

	private static final Random RANDOM = new Random();
	public static CardinalDirection pickOne()
	{
		final CardinalDirection[] choices = CardinalDirection.values();
		int index = RANDOM.nextInt(choices.length);
		return choices[index];
	}
}
