package oxymoron.everything.game;

import oxymoron.everything.game.tileSets.TileSet;
import oxymoron.everything.game.tileSets.TileType;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BlockFactory implements IBlockFactory{
    int currentHeight = 2;
    int currentX = 0;

    int abyss = 0;

    private List<Block> createColumn(int x, int y_from, int y_to) {
        List<Block> ret = new ArrayList<Block>();
        for (int i = y_from; i <= y_to; i++) {
            String tileId = TileSet.getIdString(TileType.MIDDLE);
            if (i == y_from) {
                tileId = TileSet.getIdString(TileType.TOP);
            }
            ret.add(new Block(tileId, new Vec2d(x * Block.HEIGHT, i * Block.HEIGHT)));
        }

        return ret;
    }

    public List<Block> getNextColumn() {

        List<Block> blocks = new ArrayList<Block>();
        if (abyss == 0) {
            blocks =  createColumn(currentX, currentHeight, 40);
        } else {
            abyss --;
        }

        currentX ++;

        if (Math.random() > 0.8) {
            if (Math.random() > 0.5) {
                currentHeight ++;
            } else {
                currentHeight --;
            }
        }

        if ((abyss == 0) && (Math.random() > 0.98)) {
            abyss = (int)((Math.random() * 1.0f) + 1.0f);
        }

        return blocks;
    }
}
