package oxymoron.everything.game;


/**
 * Created by are on 25.01.15.
 */
public class Transform {
    private Vec2d pos;
    private double angle;
    private Boolean flipped;

    public Vec2d getPos() { return pos; }

    public double getAngle() { return angle; }

    public void setPos(Vec2d pos) { this.pos = new Vec2d(pos.x, pos.y); }

    public void setAngle(double angle) { this.angle = angle; }

    public Boolean isFlipped() { return flipped; }

    public void setFlipped(boolean flipped) { this.flipped = flipped; }

    public void flip() { this.flipped = !this.flipped; }

    Transform(Transform t)
    {
        pos = new Vec2d(t.pos.x, t.pos.y);
        angle = t.angle;
        flipped = new Boolean(false);
    }
    Transform(Vec2d p, double angle)
    {
        setPos(p);
        setAngle(angle);
    }
    Transform()
    {
        pos = new Vec2d(0.0f, 0.0f);
        angle = 0.0f;
    }

    public Vec2d transformPoint(Vec2d p)
    {
        return new Vec2d(
                ((Math.cos(angle) * p.x) - (Math.sin(angle) * p.y)) + pos.x,
                ((Math.sin(angle) * p.x) + (Math.cos(angle) * p.y)) + pos.y
        );
    }
    public Vec2d transformVector(Vec2d p)
    {
        return new Vec2d(
                ((Math.cos(angle) * p.x) - (Math.sin(angle) * p.y)),
                ((Math.sin(angle) * p.x) + (Math.cos(angle) * p.y))
        );
    }
}
