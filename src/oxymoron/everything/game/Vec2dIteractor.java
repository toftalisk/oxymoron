package oxymoron.everything.game;

import java.util.Iterator;

public class Vec2dIteractor implements Iterable<Vec2d> {
    private Vec2d[] args;

    public Vec2dIteractor(Vec2d ... args) {
        this.args = args;
    }

    @Override
    public Iterator<Vec2d> iterator() {
        return new Iterator<Vec2d>() {
            int index = 0;
            @Override
            public boolean hasNext() {
                return index < args.length;
            }

            @Override
            public Vec2d next() {
                return args[index++];
            }

            @Override
            public void remove() {

            }
        };
    }
}
