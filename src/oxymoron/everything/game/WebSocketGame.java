package oxymoron.everything.game;

import oxymoron.everything.protocols.AssProtocol;

import java.awt.datatransfer.DataFlavor;
import java.util.*;

import java.lang.System;

public abstract class WebSocketGame {
    public enum KeyState
    {
        Depressed(true, true),
        Down(true, false),
        Released(false, true),
        Up(false, false);

        public final boolean isDown;
        public final boolean isChanged;

        KeyState(boolean isDown, boolean isChanged) {
            this.isDown = isDown;
            this.isChanged = isChanged;
        }
    }

    private final String id;
    protected AssProtocol protocol;
    Set<String> users = new HashSet<>();
    private Thread loop;


    private Map<String, String> input_fucker = new HashMap<String, String>();

    private List<String> realIds = new ArrayList<String>();

    public WebSocketGame(String id, AssProtocol protocol) {
        this.id = id;
        this.protocol = protocol;
        realIds.add("right");
        realIds.add("jump");
        realIds.add("dig");
        realIds.add("left");
        realIds.add("shootFireball");
        realIds.add("setBomb");

        for (int i = 0; i < realIds.size(); i++) {
            input_fucker.put(realIds.get(i), realIds.get(i));
        }
    }

    public abstract void onInit();
    public abstract void onUpdate(double time, double delta);

    private Map<String, KeyState> input_states = new HashMap<String, KeyState>();

    public abstract  void onScreenResize(int w, int h);

    public String getId()
    {
        return id;
    }

    public void InputDown(String id)
    {
        input_states.put(id, KeyState.Depressed);
    }
    public void InputUp(String id)
    {
        input_states.put(id, KeyState.Released);
    }

    public KeyState queryInputState(String id)
    {
        String actualInput = input_fucker.get(id);
        if (input_states.containsKey(actualInput)) {
            KeyState result = input_states.get(input_fucker.get(id));
            if (result == KeyState.Depressed)
                input_states.put(actualInput, KeyState.Down);
            if (result == KeyState.Released)
                input_states.put(actualInput, KeyState.Up);
            return result;
        } else {
            return KeyState.Up;
        }
    }



    protected void fuckYourInputs(){
        System.out.println("Fuck up inputs.");
        List<String> shuffleList = new ArrayList<String>();

        for (int i = 0; i< users.size(); i++) {
            shuffleList.add(realIds.get(i));
        }

        Collections.shuffle(shuffleList);

        for (int i = 0; i < realIds.size(); i++) {
            if (i < users.size()) {
                input_fucker.put(realIds.get(i), shuffleList.get(i));
            } else {
                input_fucker.put(realIds.get(i), realIds.get(i));
            }
        }
    }

    public void addUser(String userId) {
        users.add(userId);
    }

    public void startGame() {
        System.out.println("Starting game thread " + id);
        loop =new Thread() {
            @Override
            public void run() {
                onInit();
                double lastTime = 0;
                while(true) {
                    try {
                        double time = System.nanoTime() / 1000000000.0;

                        double delta = 0;
                        if (lastTime > 0)
                            delta = time - lastTime;
                        onUpdate(time, delta);
                        lastTime = time;

                        protocol.drawJavascript();
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        throw new RuntimeException("Something went very wrong,");
                    }
                }
            }
        };
        loop.start();
    }

    public void stop() {
        try {
            System.out.println("Stopping game " + id);
            loop.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
