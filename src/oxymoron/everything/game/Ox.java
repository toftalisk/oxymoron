package oxymoron.everything.game;

import java.util.EnumSet;

/**
 * Created by are on 25.01.15.
 */
public class Ox extends DynamicGameObject {

    private boolean onGround = false;

    public Ox(Vec2d pos) {
        super(pos);

        addCollisionFixture(new Vec2d(160,160), CollisionFixture.Layer.Enemies, EnumSet.of(CollisionFixture.Layer.Blocks, CollisionFixture.Layer.Character));
        setAffectedByGravity(true);

        getTransform().flip();

    }

    @Override
    public String getImage() {
        return "oxrun";
    }

    @Override
    public void update(WorldAction worldAction, double time, double delta)
    {
        double factor = 1.0f;
        if (getTransform().isFlipped()) {
            factor = -1.0f;
        }
        setVelocity(500.0 * factor, getVelocity().y);

        setAngle(0.0f);
        if (getVelocity().lengthSq() > 5) {

            setAngle(Math.PI - getVelocity().toAngle());
        }
    }

    public void jump()
    {
        if (onGround) {
            setVelocity(getVelocity().x, -800.0);
            System.out.println("Oxen jump");
        }
    }
    public boolean onCollision(Vec2d direction, GameObject theOtherThing)
    {
        if (Math.abs(direction.x)>Math.abs(direction.y)) {
            jump();
        } else {
            if (direction.y > 0.0) {
                if (!onGround) {
                    System.out.println("Oxen just landed.");
                }
                onGround = true;
            }
        }
        return true;
    }
}
