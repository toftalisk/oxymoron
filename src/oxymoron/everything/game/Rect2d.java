package oxymoron.everything.game;

public class Rect2d
{
	public double left;
	public double right;
	public double top;
	public double bottom;

	public Rect2d(double left, double right, double top, double bottom)
	{
		this.left = left;
		this.right = right;
		this.top = top;
		this.bottom = bottom;
	}
}
