package oxymoron.everything.game;

/**
 * Created by are on 25.01.15.
 */
public class Range {
    private double min;
    private double max;
    Range(double v)
    {
        min = v;
        max = v;
    }
    Range(double min, double max)
    {
        this.min = min;
        this.max = max;
    }
    public static Range overlap(Range a, Range b)
    {
        return new Range(Math.max(a.min, b.min), Math.min(a.max, b.max));
    }
    public double getSize()
    {
        return (max - min);
    }
    void extend(double v)
    {
        if (v < min) {
            min = v;
        }
        if (v > max) {
            max = v;
        }
    }

    public double getMax() {
        return max;
    }

    public double getMin() {
        return min;
    }
}
