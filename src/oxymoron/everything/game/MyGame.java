package oxymoron.everything.game;

import oxymoron.everything.game.gameobjects.Camera;
import oxymoron.everything.game.gameobjects.GameCharacter;
import oxymoron.everything.game.gameobjects.GameObjectMap;
import oxymoron.everything.game.tileSets.TileSet;
import oxymoron.everything.phyz.Phyzics;
import oxymoron.everything.protocols.AssProtocol;

import java.util.ArrayList;
import java.util.List;

public class MyGame extends WebSocketGame {
    private IBlockFactory blockFactory = new DigThoseBlockFactory();;

    public MyGame(String id, AssProtocol ass) {
        super(id, ass);
    }
    private Phyzics phyz = new Phyzics();

    private int updatesSinceLastJump = 0;
    private int jumps;
    private int dig = 0;

    double loaded = 0.0f;

    GameCharacter character = new GameCharacter();

    GameObjectMap<GameObject> gameObjects = new GameObjectMap<>();

    Camera camera = new Camera(new Camera.Handler() {
        @Override
        public void onCameraMove(Vec2d pos) {
            double screen_right = camera.max().x + 128;
            while (screen_right - loaded > 128.0) {
                List<Block> blocks = blockFactory.getNextColumn();

                for(Block block : blocks){
                    protocol.create(block);
                    gameObjects.add(block);
                }

                // 5% change of an oxen spawning on the tile
                if ((loaded > 2.0) && (blocks.size() > 0) && (Math.random() > 0.95)) {
                    createOxen(blocks.get(0).getPos().add(0, -256));
                }

                loaded += 128.0;
            }
            protocol.camera(pos);
        }
    });

    public void createOxen(Vec2d startPosition)
    {
        Ox ox = new Ox(startPosition);
        gameObjects.add(ox);
        protocol.create(ox);
    }
    @Override
    public void onInit() {
        protocol.register("b", "bunny.png");
        protocol.register("c", "testupsidedown.png");
        protocol.register("d", "Graphics/tileset_dirt.png");

        protocol.register("dig1", "Graphics/Animation/dig_transition_0.png");
        protocol.register("dig2", "Graphics/Animation/dig_transition_1.png");
        protocol.register("dig3", "Graphics/Animation/sheets/dig.png", 4, 8, true);

        protocol.register("fireball", "Graphics/Animation/sheets/fireball.png", 3, 15, true);
        protocol.register("plasmaball", "Graphics/Animation/sheets/plasmaball.png", 3, 15, true);
        protocol.register("still", "Graphics/Animation/sheets/idle.png", 4, 2, true);
        protocol.register("run", "Graphics/Animation/sheets/run_cycle.png", 6, 8, true);
        protocol.register("jump", "Graphics/Animation/sheets/jump.png", 2, 8, false);
        protocol.register("proton_bomb", "Graphics/Animation/sheets/proton_bomb.png", 2, 2, true);
        protocol.register("proton_explosion", "Graphics/Animation/sheets/proton_bomb_explosion.png", 5, 10, false);

        protocol.register("oxrun", "Graphics/Animation/sheets/ox.png", 4, 8, true);
        protocol.register("oxstill", "Graphics/Animation/ox.png");

        TileSet.registerTiles(protocol);

        protocol.create(character);
        gameObjects.add(character);


    }

    private void despawnOutsideCamera(List<String> toRemove) {
        Vec2d max = camera.max().add(256, 25600);
        Vec2d min = camera.min().add(-256, -25600);
        // Because we're creating 50 tiles of screen to the right
        max = max.addX(50 * 128 * 2);
        for (GameObject ob : gameObjects.values()) {
            if (!ob.getTransform().getPos().containedWithin(min, max)) {
                toRemove.add(ob.getId());
            }
        }
    }

    @Override
    public void onUpdate(double time, double delta) {
        boolean left = queryInputState("left").isDown;
        boolean right = queryInputState("right").isDown;
        if (left == right) {
            character.perform(GameCharacter.Action.Stop);
        }
        else if (right) {
            character.perform(GameCharacter.Action.MoveRight);
        }
        else
        {
            character.perform(GameCharacter.Action.MoveLeft);
        }

        if (queryInputState("shootFireball") == KeyState.Depressed)
            character.perform(GameCharacter.Action.ShootFireball);

        if (queryInputState("jump") == KeyState.Depressed) {
            character.perform(GameCharacter.Action.Jump);
        }

        if(queryInputState("dig").isDown) {
            character.perform(GameCharacter.Action.DigDown);
        }else {
            character.perform(GameCharacter.Action.StopDig);
        }

        if (queryInputState("setBomb") == KeyState.Depressed)
        {
            character.perform(GameCharacter.Action.SetBomb);
        }

        List<GameObject> toSpawn = new ArrayList<>();
        List<String> toRemove = new ArrayList<>();

        //This is shit but fine for testing
        double xDiff = (int)(character.getPos().x - camera.getPos().x);
        double yDiff = (int)(character.getPos().y - camera.getPos().y);


        //Can't walk off the left side of the screen
        int halfCameraWidth = (int)(camera.max().x-camera.getPos().x);
        if(left && (xDiff-(character.getCollisionFixtureSize().x/2) <= -halfCameraWidth)){
            character.perform(GameCharacter.Action.Stop);
        }

        //Dont move the camera if player is to the left of its center
        if(xDiff<0) xDiff = 0;

        //Dampening the y camera movement if not far from the player
        //Just tested with these random values and they seemed ok
        //yes i realize this is an awful way to do it
        // but you know what? go fuck yourself
        if(Math.abs(yDiff) <= 64) yDiff = yDiff/3;
        if(Math.abs(yDiff) <= 128 && Math.abs(yDiff) > 64) yDiff = yDiff/2;
        if(Math.abs(yDiff) <= 256 && Math.abs(yDiff) > 128) yDiff = yDiff/1.5;
        if(Math.abs(yDiff) <= 512 && Math.abs(yDiff) > 256) yDiff = yDiff/1.25;
        camera.moveToRelative(new Vec2d(xDiff, yDiff));





        for(DynamicGameObject obj : gameObjects.physical()) {
            phyz.checkCollisions(obj, gameObjects);
            if (phyz.advance(obj, time, delta)) {
                protocol.update(obj);
            }
        }

        despawnOutsideCamera(toRemove);

        for (GameObject object : gameObjects.values())
        {
            WorldAction action = new WorldAction();
            object.update(action, time, delta);
            toSpawn.addAll(action.getSpawnList());
            toRemove.addAll(action.getDestroyList());
             if (action.isImageChanged())
                 protocol.updateImage(object.getId(), action.getNewImage());
        }

        if (character.getShouldFuckUp()) {
            fuckYourInputs();
        }

        for (GameObject obj: toSpawn) {
            protocol.create(obj);
            gameObjects.add(obj);
        }

        for (String id: toRemove) {
            protocol.delete(id);
            gameObjects.remove(id);
        }
    }

    @Override
    public void onScreenResize(int w, int h) {
        camera.setSize(w,h);
    }
}
