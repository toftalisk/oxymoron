package oxymoron.everything.game;

import oxymoron.everything.utils.IdGen;

import java.util.EnumSet;

public abstract class GameObject
{
    private static final IdGen idgen = new IdGen();
	private final String id = idgen.next();
    private CollisionFixture collisionFixture;
    private Transform transform;

    public boolean isCollidable() { return (collisionFixture != null); }

    public Vec2d getPos() { return transform.getPos(); }

    public void setAngle(double v) { transform.setAngle(v);}

    public Vec2d getCollisionFixtureSize(){
        return collisionFixture.getSize();
    }

    public GameObject(Vec2d pos) {
        setTransform(new Transform(pos, 0.0));
    }

    public CollisionFixture getCollisionFixture() { return collisionFixture; }

    public void addCollisionFixture(Vec2d size, CollisionFixture.Layer layer, EnumSet<CollisionFixture.Layer> mask) {
        this.collisionFixture = CollisionFixture.createBox(size, layer, mask);
    }

    public final String getId()
	{
		return id;
	}



    public abstract String getImage();

    public Transform getTransform() { return transform; }
    public void setTransform(Transform t) { transform = new Transform(t); }

    public void update(WorldAction action, double time, double delta) {}
}
