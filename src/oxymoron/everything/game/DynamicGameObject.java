package oxymoron.everything.game;


public abstract class DynamicGameObject extends GameObject {
	private Vec2d velocity;
	private boolean affectedByGravity;
	private double invMass;

    public DynamicGameObject(Vec2d pos) {
        super(pos);
		velocity = new Vec2d(0.0, 0.0f);
		invMass = 1.0;
    }

	public boolean onCollision(Vec2d direction, GameObject theOtherThing) { return true; }

	public double getInvMass() { return invMass; }

	public Vec2d getVelocity()
	{
		return velocity;
	}

	public void addVelocity(Vec2d velocity) { this.velocity = this.velocity.add(velocity); }
	public void setVelocity(Vec2d velocity) {
		this.velocity = velocity;
	}

	public boolean isAffectedByGravity()
	{
		return affectedByGravity;
	}

	public void setAffectedByGravity(boolean affectedByGravity)
	{
		this.affectedByGravity = affectedByGravity;
	}

    @Override
    public String toString() {
        return "vec=" + velocity + " pos=" + getTransform().getPos();
    }

    public void setVelocity(double x, double y) {
        setVelocity(new Vec2d(x, y));
    }

    public void move(Vec2d moveBy) {
        getTransform().setPos(getTransform().getPos().add(moveBy));
    }

    public void accelBy(int x, double y) {
        setVelocity(getVelocity().add(x,y));
    }
}
