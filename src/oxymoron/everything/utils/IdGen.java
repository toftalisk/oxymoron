package oxymoron.everything.utils;

public class IdGen {
    private int id = 0;

    public String next() {
        return Integer.toString(id++);
    }
}
