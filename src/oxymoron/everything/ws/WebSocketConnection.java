package oxymoron.everything.ws;


import org.eclipse.jetty.websocket.api.RemoteEndpoint;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketAdapter;
import org.eclipse.jetty.websocket.api.WriteCallback;
import oxymoron.everything.game.*;
import oxymoron.everything.protocols.AssProtocol;
import oxymoron.everything.utils.IdGen;

import static java.lang.System.*;

public class WebSocketConnection extends WebSocketAdapter implements AssProtocol
{
    private static IdGen ids = new IdGen();

    public final String userId = ids.next();

    private boolean isHost;
    private WebSocketGame game;

    @Override
    public void onWebSocketConnect(Session sess)
    {
        super.onWebSocketConnect(sess);
        out.println("New connection " + userId + "  accepted. " + this);
        Users.add(userId, this);
        isHost = false;
    }

    @Override
    public void onWebSocketText(String message)
    {
        super.onWebSocketText(message);

        String[] messages = message.trim().split(":");
        switch (messages[0]) {
            case "gameplz" :
                game = GameRepo.create(this);
                isHost = true;
                send("created:" + game.getId());
                break;
            case "joinplz" :
                String gameId = messages[1];
                isHost = false;
                game = GameRepo.join(userId, gameId);
                boolean joined = game != null;
                send(joined ? "joined:" + userId : "failed:-1");
                break;
            case "inputdown" :
                game.InputDown(messages[1]);
                out.println("Input down: " + messages[1]);
                break;
            case "inputup" :
                game.InputUp(messages[1]);
                out.println("Input up: " + messages[1]);
                break;
            case "viewport":
                if (!isHost) break;
                try {
                    int width = Integer.parseInt(messages[1]);
                    int height = Integer.parseInt(messages[2]);


                    game.onScreenResize(width, height);

                    out.println("INFO: Screen viewport resized.");
                } catch (NumberFormatException e)
                {
                    out.println("ERROR: Failed to parse viewport size.");
                }
                break;
            default: out.println("ERRROR : Dont know what to do\n" + message);
        }
    }


    WriteCallbackLog log = new WriteCallbackLog();
    private void send(final String message) {
        RemoteEndpoint remote = getRemote();
        if(remote != null) {
            remote.sendString(message, log);
        }
        else {
            System.out.println("remote connection is closed " + this);
            game.stop();
        }
    }

    @Override
    public void onWebSocketClose(int statusCode, String reason)
    {
        super.onWebSocketClose(statusCode,reason);
        System.out.println("Socket closed.");
        Users.drop(userId);
        game.stop();
    }

    @Override
    public void onWebSocketError(Throwable cause)
    {
        super.onWebSocketError(cause);
        cause.printStackTrace(err);
    }

    public void register(String id, String imagePath)
    {
        register(id, imagePath, 1, 0, true);
    }

    @Override
    public void register(String id, String imagePath, int frames, int fps, boolean loop) {

        send(MessageEncorder.encode("register", id, imagePath, frames, fps, loop ? 1 : 0));
    }

    @Override
    public void delete(String id) {
        send(MessageEncorder.encode("delete", id));
    }

    @Override
    public void create(GameObject o) {
            send(MessageEncorder.encode(
                    "create",
                    o.getId(),
                    o.getImage(),
                    o.getTransform().getPos().x,
                    o.getTransform().getPos().y,
                    o.getTransform().getAngle(),
                    o.getTransform().isFlipped()));
    }

    @Override
    public void update(GameObject o) {
        sendEncoded("update", o.getId(), o.getTransform().getPos().x, o.getTransform().getPos().y, o.getTransform().getAngle(), o.getTransform().isFlipped());
    }

    @Override
    public void drawJavascript() {
        sendEncoded("draw");
    }

    @Override
    public void updateImage(String objId, String image)
    {
        sendEncoded("sprite", objId, image);
    }

    @Override
    public void camera(Vec2d pos) {
        sendEncoded("camera", pos.x, pos.y);
    }

    private void sendEncoded(Object ... args) {
        send(MessageEncorder.encode(args));
    }
}
