package oxymoron.everything.ws;

import org.eclipse.jetty.websocket.api.WriteCallback;

import static java.lang.System.*;

public class WriteCallbackLog implements WriteCallback {
    @Override
    public void writeFailed(Throwable x) {
        out.println("Un caught exception while sending on web socket:");
        out.println(x);
    }

    @Override
    public void writeSuccess() { }
}
