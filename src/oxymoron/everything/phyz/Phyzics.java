package oxymoron.everything.phyz;

import oxymoron.everything.game.*;
import oxymoron.everything.game.gameobjects.GameObjectMap;

public class Phyzics {
    private static final double GRAVITY = 900;

    public Phyzics() {}

    public boolean advance(DynamicGameObject obj, double time, double delta) {
        Vec2d vel = obj.getVelocity();

        applyGravity(obj, time, delta);

        if (vel == null || (vel.x == 0 && vel.y == 0)) return false;

        Vec2d moveBy = new Vec2d(vel.x * delta, vel.y * delta);

        obj.move(moveBy);

        return true;
    }

    private void applyGravity(DynamicGameObject obj, double time, double delta) {
        if(!obj.isAffectedByGravity()) return;

        obj.accelBy(0, GRAVITY * delta);
    }

    public void checkCollisions(DynamicGameObject obj, GameObjectMap<GameObject> gameObjects) {
        if(!obj.isCollidable()) return;

        for(GameObject other : gameObjects.values()) {
            if (obj == other) continue;
            if (!other.isCollidable()) continue;

            CollisionFixture objFixture = obj.getCollisionFixture();
            CollisionFixture otherFixture = other.getCollisionFixture();

            if (!objFixture.canCollideWith(otherFixture)) continue;

            SATResult sat = CollisionFixture.getAxisOfSeparation(obj.getCollisionFixture(), obj.getTransform(), other.getCollisionFixture(), other.getTransform());

            // Objects are colliding
            if (sat != null) {
                Vec2d v2 = new Vec2d(0.0, 0.0);
                double m_inv2 = 0.0;
                if (other instanceof DynamicGameObject) {
                    v2 = ((DynamicGameObject) other).getVelocity();
                    m_inv2 = ((DynamicGameObject) other).getInvMass();
                }

                double velocityDiff = sat.getAxis().dot(obj.getVelocity()) - sat.getAxis().dot(v2);

                if (velocityDiff < 0.0f) continue;

                if (obj.onCollision(sat.getAxis(), other)) {
                    double impulse = velocityDiff / (obj.getInvMass() + m_inv2);

                    obj.addVelocity(sat.getAxis().mul(impulse * -obj.getInvMass()));
                    obj.move(sat.getAxis().mul(-sat.getOverlap().getSize()));
                    if (other instanceof DynamicGameObject) {
                        ((DynamicGameObject) other).addVelocity(sat.getAxis().mul(impulse * m_inv2));
                    }
                }
            }

        }
    }
}
