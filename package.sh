gradle clean installApp
cp public/ build/install/oxymoron/bin/ -R
cp README build/install/oxymoron/
cp scripts/ox.sh build/install/oxymoron/
cd build/install/oxymoron/
zip ox.zip * -r
cd ../../../
mv build/install/oxymoron/ox.zip build/
echo "Made build/ox.zip"
